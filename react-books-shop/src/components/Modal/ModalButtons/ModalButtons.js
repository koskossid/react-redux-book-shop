import React from 'react';
import Button from "../../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {toggleModalAction} from "../../../store/modal/modalAction";
import {toggleInCartAction} from "../../../store/books/booksListAction";

const ModalButtons = (props) => {
    const activeModalCard = useSelector(state => state.modalState.activeModalCard)
    const dispatch = useDispatch()
    return (
        <div>
            <Button onClickBtn={() => {
                dispatch(toggleInCartAction(activeModalCard))
                dispatch(toggleModalAction())
            }}>
                Ok
            </Button>
            <Button
                onClickBtn={() => dispatch(toggleModalAction())}>
                Cancel
            </Button>
        </div>
    );
}

export default ModalButtons;