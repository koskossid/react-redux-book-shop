import React from 'react';
import classes from './CloseModalButton.module.scss';
import {useDispatch} from "react-redux";
import {toggleModalAction} from "../../../store/modal/modalAction";

const CloseModalButton = () => {
    const dispatch = useDispatch();
    const cls = [classes.CloseModalButton, 'fa fa-times'];
    return (
        <i className={cls.join(' ')} onClick={() => dispatch(toggleModalAction())}/>
    );
}

export default CloseModalButton;