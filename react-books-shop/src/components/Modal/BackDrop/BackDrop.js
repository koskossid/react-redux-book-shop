import React from 'react';
import classes from './BackDrop.module.scss';
import {useDispatch} from "react-redux";
import {toggleModalAction} from "../../../store/modal/modalAction";

const BackDrop = () => {
    const dispatch = useDispatch();
    return (
        <div
            className={classes.BackDrop}
            onClick={() => dispatch(toggleModalAction())}
        />
    );
}

export default BackDrop;