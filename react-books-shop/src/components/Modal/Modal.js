import React from 'react';
import classes from './Modal.module.scss';
import BackDrop from "./BackDrop/BackDrop";
import CloseModalButton from "./CloseModalButton/CloseModalButton";
import ModalButtons from "./ModalButtons/ModalButtons";
import {useSelector} from "react-redux";
import {modalConstInput} from "./ModalConstInput";

const Modal = () => {
    const activeModalCard = useSelector(state => state.modalState.activeModalCard)
    const {addToCart, removeFromCart} = modalConstInput;
    let modalHeader, modalText;
    if (activeModalCard) {
        if (JSON.parse(localStorage.getItem("inCartStorage")) && JSON.parse(localStorage.getItem("inCartStorage")).includes(activeModalCard)) {
            modalHeader = removeFromCart.modalHeader
            modalText = removeFromCart.modalText
        } else {
            modalHeader = addToCart.modalHeader
            modalText = addToCart.modalText
        }
    }
    return (
        <React.Fragment>
            <div className={classes.Modal}>
                <div className={classes.Modal__title}>
                    <h2><strong>{modalHeader}</strong></h2>
                    <CloseModalButton/>
                </div>
                <div className={classes.Modal__text}>
                    <p>{modalText}</p>
                    <ModalButtons/>
                </div>
            </div>
            <BackDrop/>
        </React.Fragment>
    );
}

export default Modal;