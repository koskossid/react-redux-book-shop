
export const modalConstInput = {
    addToCart: {
        modalHeader: 'Add to cart',
        modalText: 'Confirm you want to add book to cart',
    },
    removeFromCart: {
        modalHeader: 'Remove from cart',
        modalText: 'Confirm you want to remove book from cart',
    }
}

