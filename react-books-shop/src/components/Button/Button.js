import React from 'react';
import classes from './Button.module.scss';
import PropTypes from 'prop-types';

const Button = (props) => {
    const cls = [classes.Button];
    const {backgroundColor, onClickBtn} = props;
    return (
        <button
            className={cls.join(' ')}
            style={{backgroundColor}}
            onClick={onClickBtn}
        >
            {props.children}
        </button>
    );

}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    onClickBtn: PropTypes.func,
}

Button.defaultProps = {
    backgroundColor: 'orange',
}


export default Button;