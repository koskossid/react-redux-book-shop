import React from 'react';
import classes from './Stars.module.scss';
import PropTypes from 'prop-types';
import {useDispatch} from "react-redux";
import {toggleInFavoriteAction} from "../../../../store/books/booksListAction";

const Stars = (props) => {
    const {id, inFavorite} = props;
    const dispatch = useDispatch();
    const cls = [classes.Stars, 'fas fa-star'];
    let starColor
    if (inFavorite) {
        starColor = 'orange'
    } else {
        starColor = 'black'
    }
    return (
        <i onClick={() => dispatch(toggleInFavoriteAction(id))} className={cls.join(' ')} style={{color: starColor}}></i>
    )
}

Stars.propTypes = {
    inFavorite: PropTypes.bool,
    id: PropTypes.number
}

export default Stars;