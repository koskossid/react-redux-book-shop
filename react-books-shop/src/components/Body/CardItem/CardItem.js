import React from 'react';
import classes from './CardItem.module.scss'
import Stars from "./Stars/Stars";
import CardButton from "./CardButton/CardButton";
import {toggleModalAction} from "../../../store/modal/modalAction";
import {useDispatch} from "react-redux";
import PropTypes from 'prop-types'

const CardItem = (props) => {
    const dispatch = useDispatch()
    const {book: {name, price, imgUrl, id, color, inCart, inFavorite}, deleteButtonToShowStatus} = props;
    const cls = [classes.cardTimesButton, 'fa fa-times'];
    let buttonText, buttonColor;
    if (inCart) {
        buttonText = 'Book in Cart'
        buttonColor = 'green'
    } else {
        buttonText = 'Add to Cart'
        buttonColor = color
    }
    return (
        <div className={classes.CardItem}>
            <div className={classes.cardImg}>
                <img src={imgUrl} alt="card_image"></img>
            </div>
            <div className={classes.cardDescription}>
                <p>id({id}): {name} {<Stars id={id} inFavorite={inFavorite}/>}</p>
                <div className={classes.cardPrice}>
                    <p><strong>Price {price}$</strong></p>
                    <CardButton backgroundColor={buttonColor} cardID={id} buttonText={buttonText}/>
                </div>
            </div>
            {deleteButtonToShowStatus && <i className={cls.join(' ')} onClick={() => dispatch(toggleModalAction(id))}/>}
        </div>
    );
}

CardItem.propTypes = {
    book: PropTypes.exact({
        name: PropTypes.string,
        price: PropTypes.number,
        imgUrl: PropTypes.string,
        id: PropTypes.number,
        color: PropTypes.string,
        inCart: PropTypes.bool,
        inFavorite: PropTypes.bool
    }),
    deleteButtonToShowStatus: PropTypes.bool
}


export default CardItem;