import React from 'react';
import Button from "../../../Button/Button";
import {useDispatch} from "react-redux";
import {toggleModalAction} from "../../../../store/modal/modalAction";
import PropTypes from "prop-types";

const CardButton = (props) => {
    const dispatch = useDispatch()
    const {backgroundColor, cardID, buttonText} = props
    return (
        <Button
            onClickBtn={() => dispatch(toggleModalAction(cardID))}
            backgroundColor={backgroundColor}>
            {buttonText}
        </Button>
    );
}

CardButton.propTypes = {
    backgroundColor: PropTypes.string,
    cardID: PropTypes.number,
    buttonText: PropTypes.string
}


export default CardButton;