import React from 'react';
import classes from './Loader.module.scss'

const Loader = () => {
    return (
        <div className={classes.Loader}>
            <h1>Request data from server ...</h1>
        </div>
    );
}

export default Loader;