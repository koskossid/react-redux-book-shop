import React from 'react';
import classes from './Body.module.scss';
import Loader from "./Loader/Loader";
import CardItem from "./CardItem/CardItem";
import {useSelector} from "react-redux";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

const Body = (props) => {
    const modalIsOpen = useSelector(state => state.modalState.modalIsOpen)
    const {list, deleteButtonToShowStatus} = props;
    if (!list) {
        return <Loader/>
    }
    return (
        <div className={classes.Body}>
            <main className={classes.booksContainer}>
                {list.map((book, index) => {
                    return (
                        <CardItem key={index} book={book} deleteButtonToShowStatus={deleteButtonToShowStatus}/>
                    )
                })}
            </main>
            {modalIsOpen ?
                <Modal/>
                : null
            }
        </div>
    )
}

Body.propTypes = {
    deleteButtonToShowStatus: PropTypes.bool,
    list:PropTypes.array
}

export default Body;