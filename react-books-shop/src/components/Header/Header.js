import React from 'react';
import classes from './Header.module.scss';
import {NavLink} from 'react-router-dom'

const Header = () => {
    return (
        <div className={classes.Header}>
            <nav>
                <ul>
                    <li>
                        <NavLink to="/" exact activeClassName={classes.active}>Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/cart" activeClassName={classes.active}>Cart</NavLink>
                    </li>
                    <li>
                        <NavLink to="/favorites" activeClassName={classes.active}>Favorites</NavLink>
                    </li>
                </ul>
            </nav>
        </div>
    );
}

export default Header;