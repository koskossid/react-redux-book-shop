import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {getListFromServer} from "./store/books/booksListAction";
import Header from "./components/Header/Header";
import Router from "./Router/Router";
import PropTypes from 'prop-types'


const App = (props) => {
    const {getBooks} = props;
    useEffect(() => {
        getBooks();
    }, [getBooks])

    return (
        <div>
            <Header/>
            <Router/>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        getBooks: () => dispatch(getListFromServer())
    }
}

App.propTypes = {
    getBooks: PropTypes.func
}

export default connect(null, mapDispatchToProps)(App);
