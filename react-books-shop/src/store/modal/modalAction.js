
import {TOGGLE_MODAL} from '../actionTypes'

export const toggleModalAction = (id) => {
    return {
        type: TOGGLE_MODAL,
        payload: id
    }
}
