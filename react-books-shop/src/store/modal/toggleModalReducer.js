
import {TOGGLE_MODAL} from '../actionTypes'

const initialState = {
    modalIsOpen: false,
    activeModalCard: null
}

const toggleModalReducer = (state = initialState, action) => {
    const id = action.payload ? action.payload : null;
    switch (action.type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                modalIsOpen: !state.modalIsOpen,
                activeModalCard: id
            }
        default:
            return state
    }
}

export default toggleModalReducer;
