import {ADD_BOOKS_LIST, TOGGLE_IN_FAVORITE, TOGGLE_IN_CART} from '../actionTypes'
import axios from 'axios'

export const addBooksList = (list) => ({
    type: ADD_BOOKS_LIST,
    payload: list
})

export const getListFromServer = () => {
    return dispatch => {
        setTimeout(() => {
            return  axios('./jsonData.json')
                .then((list) => {
                    let booksList = [...list.data];
                    const inFavoriteID = JSON.parse(localStorage.getItem("inFavoriteStorage"));
                    const inCartID = JSON.parse(localStorage.getItem("inCartStorage"));
                    booksList.forEach((record) => {
                        if (inFavoriteID && inFavoriteID.includes(record.id)) record.inFavorite = true;
                        if (inCartID && inCartID.includes(record.id)) record.inCart = true
                    })
                    return booksList
                })
                .then(list => dispatch(addBooksList(list)))
        }, 1500)
    }
}

export const toggleInFavoriteAction = (id) => ({
    type: TOGGLE_IN_FAVORITE,
    payload: id
})

export const toggleInCartAction = (id) => ({
    type: TOGGLE_IN_CART,
    payload: id
})

