import {ADD_BOOKS_LIST, TOGGLE_IN_CART, TOGGLE_IN_FAVORITE} from '../actionTypes'

const initialState = {
    booksList: null
}

const booksListReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_BOOKS_LIST:
            return {
                ...state,
                booksList: action.payload
            }
        case TOGGLE_IN_FAVORITE:
            const favoriteBooksList = [...state.booksList];
            const idFavorite = action.payload
            favoriteBooksList.forEach(book => {
                if (book.id === idFavorite) book.inFavorite = !book.inFavorite
            })
            const idInFavorite = favoriteBooksList.filter(book => book.inFavorite).map(book => book.id);
            localStorage.setItem('inFavoriteStorage', JSON.stringify(idInFavorite));
            return {
                ...state,
                booksList: favoriteBooksList
            }
        case TOGGLE_IN_CART:
            const cartBooksList = [...state.booksList];
            const idCart = action.payload
            cartBooksList.forEach(book => {
                if (book.id === idCart) book.inCart = !book.inCart
            })
            const idInCart = cartBooksList.filter(book => book.inCart).map(book => book.id);
            localStorage.setItem('inCartStorage', JSON.stringify(idInCart));
            return {
                ...state,
                booksList: cartBooksList
            }
        default:
            return state
    }
}

export default booksListReducer;
