export const ADD_BOOKS_LIST = 'ADD_BOOKS_LIST';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';
export const TOGGLE_IN_FAVORITE = 'TOGGLE_IN_FAVORITE';
export const TOGGLE_IN_CART = 'TOGGLE_IN_CART';