import {combineReducers} from 'redux';
import booksListReducer from "./books/booksListReducer";
import toggleModalReducer from "./modal/toggleModalReducer";

const rootReducer = combineReducers({
    books: booksListReducer,
    modalState: toggleModalReducer
})

export default rootReducer;