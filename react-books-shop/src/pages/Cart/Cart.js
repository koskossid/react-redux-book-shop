import React from "react";
import {useSelector} from "react-redux";
import Body from "../../components/Body/Body";
import classes from './Cart.module.scss'

const Cart = () => {
    const booksList = useSelector(state => state.books.booksList);
    let list;
    if (booksList) list = [...booksList].filter(book => book.inCart)
    return (
        <div>
            {(!list || list.length === 0) && <h1 className={classes.CartEmpty}>Cart is Empty</h1>}
            <Body list={list} deleteButtonToShowStatus={true}/>
        </div>
    )
}

export default Cart;