import React from "react";
import {useSelector} from "react-redux";
import Body from "../../components/Body/Body";
import classes from "./Favorites.module.scss";

const Favorites = () => {
    const booksList = useSelector(state => state.books.booksList);
    let list;
    if (booksList) list = [...booksList].filter(book => book.inFavorite)
    return (
        <div>
            {(!list || list.length === 0) && <h1 className={classes.FavoritesEmpty}>Favorites are Empty</h1>}
            <Body list={list} deleteButtonToShowStatus={false}/>
        </div>
    )
}

export default Favorites;