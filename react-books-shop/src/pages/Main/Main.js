import React from "react";
import Body from "../../components/Body/Body";
import {useSelector} from "react-redux";

const Main = () => {
    const booksList = useSelector(state => state.books.booksList);
    return (
        <Body list={booksList} deleteButtonToShowStatus={false}/>
    )
}
export default Main;