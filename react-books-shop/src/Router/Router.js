import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Main from "../pages/Main/Main";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";

const Router = () => {
    return (
            <Switch>
                <Route exact path='/' component={Main}/>
                <Route exact path='/cart' component={Cart}/>
                <Route exact path='/favorites' component={Favorites}/>
                <Redirect to='/'/>
            </Switch>
    );
}

export default Router;